import React, { Component, Fragment } from "react";
import Dashboard from "container/Dashboard.js";
import Notification from "container/Notification.js";

class App extends Component {
  render() {
    return (
      <Fragment>
        <Dashboard />
        <Notification />
      </Fragment>
    );
  }
}

export default App;
