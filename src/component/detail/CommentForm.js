import React, { Component } from "react";
import Paper from "material-ui/Paper";
import styled from "styled-components";
import TextField from "material-ui/TextField";
import Button from "material-ui/Button";
import { CircularProgress } from "material-ui/Progress";
import { isEmpty } from "module/ValidationUtils.js";
import { maxLengthOfName, maxLengthOfComment } from "module/Constants.js";
import Divider from "material-ui/Divider";
import { primary as primaryColor } from "module/Colors.js";

const PaperStyled = styled(Paper)`
  padding: 15px 50px;
  text-align: center;
  color: ${primaryColor};
`;

const FullWidthButton = styled(Button)`
  width: 100%;
  margin-top: 15px;
  margin-bottom: 10px;
  && {
    border-radius: 20px;
  }
`;

const DividerStyled = styled(Divider)`
  && {
    margin-bottom: 10px;
  }
`;

class CommentForm extends Component {
  constructor() {
    super();

    this.state = {
      firstName: "",
      lastName: "",
      comment: "",
      isCreatingComment: false
    };
  }

  isAddButtonEnabled() {
    return (
      !isEmpty(this.state.firstName) &&
      !isEmpty(this.state.lastName) &&
      !isEmpty(this.state.comment)
    );
  }

  submitForm() {
    this.setState({ isCreatingComment: true });
    if (this.state.isCreatingComment) {
      return;
    }
    this.props
      .onSubmit(this.state.firstName, this.state.lastName, this.state.comment)
      .then(() => {
        this.setState({
          isCreatingComment: false,
          firstName: "",
          lastName: "",
          comment: ""
        });
      })
      .catch(() => {
        this.setState({ isCreatingComment: false });
      });
  }

  render() {
    return (
      <PaperStyled>
        <h2>Add comment</h2>
        <DividerStyled />
        <TextField
          label="First Name"
          margin="normal"
          fullWidth
          value={this.state.firstName}
          onChange={event => {
            this.setState({
              firstName: event.target.value.substring(0, maxLengthOfName)
            });
          }}
        />
        <TextField
          label="Last Name"
          margin="normal"
          fullWidth
          value={this.state.lastName}
          onChange={event => {
            this.setState({
              lastName: event.target.value.substring(0, maxLengthOfName)
            });
          }}
        />
        <TextField
          label="Comment..."
          multiline
          margin="normal"
          fullWidth
          value={this.state.comment}
          onChange={event => {
            this.setState({
              comment: event.target.value.substring(0, maxLengthOfComment)
            });
          }}
        />
        <FullWidthButton
          disabled={!this.isAddButtonEnabled()}
          raised
          color="accent"
          onClick={() => {
            this.submitForm();
          }}
        >
          {this.state.isCreatingComment ? (
            <CircularProgress color="primary" size={20} />
          ) : null}
          &nbsp;Add comment
        </FullWidthButton>
      </PaperStyled>
    );
  }
}

export default CommentForm;
