import React, { Component } from "react";
import Grid from "material-ui/Grid";
import Divider from "material-ui/Divider";
import IconButton from "material-ui/IconButton";
import ConfirmModal from "component/utils/ConfirmModal.js";
import styled from "styled-components";
import {
  primary as primaryColor,
  tertiary as tertiaryColor
} from "module/Colors.js";
import AvatarLoader from "component/utils/AvatarLoader.js";
import Tooltip from "material-ui/Tooltip";
var dateFormat = require("dateformat");

const PrimaryColorWrapper = styled.span`
  color: ${primaryColor};
`;

const TertiaryColorWrapper = styled.span`
  color: ${tertiaryColor};
`;

const AlignLeftWrapper = styled.div`
  text-align: left;
`;

const AlignRightWrapper = styled.div`
  text-align: right;
`;

const WordWrapDiv = styled.div`
  word-wrap: break-word;
`;

class CommentItem extends Component {
  constructor() {
    super();

    this.state = {
      isConfirmDialogOpen: false
    };
  }
  handleDeleteComment() {
    this.props
      .onDeleteRequest()
      .then(() => {
        this.setState({ isConfirmDialogOpen: false });
      })
      .catch(error => {
        this.setState({ isConfirmDialogOpen: false });
      });
  }

  render() {
    return (
      <Grid container>
        <Grid item xs={3} sm={2} lg={1}>
          <AvatarLoader src={this.props.avatarSource} />
        </Grid>
        <Grid item xs={7} sm={8} lg={9}>
          <AlignLeftWrapper>
            <PrimaryColorWrapper>
              <WordWrapDiv>
                <strong>
                  {this.props.firstName} {this.props.lastName}
                </strong>
              </WordWrapDiv>
            </PrimaryColorWrapper>
            <div>
              <TertiaryColorWrapper>
                {dateFormat(this.props.timestamp, "dd mmm  yyyy, HH:MM")}
              </TertiaryColorWrapper>
            </div>
          </AlignLeftWrapper>
        </Grid>
        <Grid item xs={2}>
          <AlignRightWrapper>
            <Tooltip title="Delete" placement="left">
              <IconButton
                color="accent"
                onClick={() => {
                  this.setState({ isConfirmDialogOpen: true });
                }}
              >
                <i className="material-icons">delete_forever</i>
              </IconButton>
            </Tooltip>
          </AlignRightWrapper>
        </Grid>
        <Grid item xs={3} sm={2} lg={1} />
        <Grid item xs={9} sm={10} lg={11}>
          <WordWrapDiv>
            <AlignLeftWrapper>
              <PrimaryColorWrapper>{this.props.comment}</PrimaryColorWrapper>
            </AlignLeftWrapper>
          </WordWrapDiv>
        </Grid>
        <Grid item xs={12}>
          <Divider />
        </Grid>
        <Divider />
        <ConfirmModal
          message="Are you sure you want delete this comment?"
          open={this.state.isConfirmDialogOpen}
          onClose={() => {
            this.setState({ isConfirmDialogOpen: false });
          }}
          onConfirm={() => {
            this.handleDeleteComment();
          }}
        />
      </Grid>
    );
  }
}

export default CommentItem;
