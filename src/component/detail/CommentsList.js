import React, { Component } from "react";
import Paper from "material-ui/Paper";
import styled from "styled-components";
import CommentItem from "component/detail/CommentItem.js";
import Divider from "material-ui/Divider";
import { primary as primaryColor } from "module/Colors.js";

const PaperStyled = styled(Paper)`
  padding: 15px;
  margin-top: 20px;
  padding-top: 5px;
  text-align: center;
  color: ${primaryColor};
`;

const DividerStyled = styled(Divider)`
  && {
    margin-bottom: 10px;
  }
`;

class CommentList extends Component {
  render() {
    if (this.props.comments.length === 0) {
      return (
        <PaperStyled>
          <h3>No comments are added</h3>
        </PaperStyled>
      );
    }
    return (
      <PaperStyled>
        <h2>Comments</h2>
        <DividerStyled />
        {this.props.comments.map((comment, index) => {
          return (
            <CommentItem
              onDeleteRequest={() => {
                return this.props.onItemDeleteRequest(comment._id);
              }}
              key={comment._id}
              avatarSource={comment.person.avatar}
              firstName={comment.person.firstName}
              lastName={comment.person.lastName}
              comment={comment.text}
              timestamp={comment.person.date}
            />
          );
        })}
      </PaperStyled>
    );
  }
}

export default CommentList;
