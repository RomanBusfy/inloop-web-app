import React, { Component } from "react";
import Grid from "material-ui/Grid";
import UserCard from "component/detail/User.js";
import CommentAdd from "container/CommentAdd.js";
import CommentsList from "container/CommentsList.js";
import styled from "styled-components";

const ContentWrapper = styled.div`
  padding: 15px;
`;

class DetailCard extends Component {
  render() {
    const feedDetail = this.props.feedDetail;
    return (
      <ContentWrapper>
        {" "}
        <Grid container justify="center" spacing={24}>
          <Grid item xs={12} sm={10} md={6}>
            <UserCard
              avatarSource={feedDetail.person.avatar}
              firstName={feedDetail.person.firstName}
              lastName={feedDetail.person.lastName}
              userName={feedDetail.person.username}
              text={feedDetail.text}
              likes={feedDetail.likesCount}
              comments={feedDetail.comments.length}
            />
          </Grid>
          <Grid item xs={12} sm={10} md={6}>
            <CommentAdd
              commentWasAdded={() => {
                this.props.commentWasAdded();
              }}
              id={feedDetail._id}
            />
            <CommentsList
              feedId={feedDetail._id}
              comments={feedDetail.comments}
              commentWasDeleted={() => {
                this.props.commentWasDeleted();
              }}
            />
          </Grid>
        </Grid>
      </ContentWrapper>
    );
  }
}

export default DetailCard;
