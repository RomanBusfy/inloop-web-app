import React, { Component } from "react";
import Paper from "material-ui/Paper";
import Divider from "material-ui/Divider";
import styled from "styled-components";
import {
  primary as primaryColor,
  tertiary as tertiaryColor,
  additional as additionalColor
} from "module/Colors.js";
import AvatarLoader from "component/utils/AvatarLoader.js";

const PaperStyled = styled(Paper)`
  padding: 15px 30px;
  text-align: center;
`;

const PrimaryColorWrapper = styled.span`
  color: ${primaryColor};
`;

const TertiaryColorWrapper = styled.span`
  color: ${tertiaryColor};
`;

const AdditionalColorWrapper = styled.span`
  color: ${additionalColor};
`;

const TextWrap = styled.div`
  text-align: left;
  font-size: 1em;
  margin-top: 15px;
  color: ${primaryColor};
`;

const NumbersWrapper = styled.div`
  margin-top: 20px;
  text-align: right;
  color: ${primaryColor};
`;

class UserCard extends Component {
  render() {
    return (
      <PaperStyled>
        <AvatarLoader src={this.props.avatarSource} />
        <h1>
          <PrimaryColorWrapper>{`${this.props.firstName} ${
            this.props.lastName
          }`}</PrimaryColorWrapper>
        </h1>
        <h2>
          <TertiaryColorWrapper>{this.props.userName}</TertiaryColorWrapper>
        </h2>
        <Divider />
        <TextWrap>{this.props.text}</TextWrap>
        <NumbersWrapper>
          <AdditionalColorWrapper>
            Likes: {this.props.likes} <i className="material-icons">favorite</i>
          </AdditionalColorWrapper>
          &nbsp;&nbsp; Comments: {this.props.comments}{" "}
          <i className="material-icons">chat</i>
        </NumbersWrapper>
      </PaperStyled>
    );
  }
}

export default UserCard;
