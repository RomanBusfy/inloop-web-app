import React, { Component } from "react";
import Paper from "material-ui/Paper";
import Grid from "material-ui/Grid";
import IconButton from "material-ui/IconButton";
import styled from "styled-components";
import { history } from "module/History.js";
import {
  primary as primaryColor,
  tertiary as tertiaryColor
} from "module/Colors.js";
import AvatarLoader from "component/utils/AvatarLoader.js";
import Tooltip from "material-ui/Tooltip";
var dateFormat = require("dateformat");

const PaperStyled = styled(Paper)`
  padding: 15px;
`;

const PrimaryColorWrapper = styled.span`
  color: ${primaryColor};
`;

const TertiaryColorWrapper = styled.span`
  color: ${tertiaryColor};
`;

const AlignLeftWrapper = styled.div`
  text-align: left;
`;

const AlignRightWrapper = styled.div`
  text-align: right;
`;

const TextWrapper = styled.div`
  max-height: 150px;
  height: 150px;
  overflow-y: auto;
`;

class Card extends Component {
  render() {
    return (
      <Grid item xs={12} sm={6} md={4}>
        <PaperStyled>
          <Grid container>
            <Grid item xs={3} sm={2} lg={2}>
              <AvatarLoader src={this.props.avatarSource} />
            </Grid>
            <Grid item xs={6} sm={7} lg={8}>
              <AlignLeftWrapper>
                <PrimaryColorWrapper>
                  <strong>
                    {this.props.firstName} {this.props.lastName}
                  </strong>
                </PrimaryColorWrapper>
                <div>
                  <TertiaryColorWrapper>
                    {dateFormat(this.props.timestamp, "dd mmm  yyyy, HH:MM")}
                  </TertiaryColorWrapper>
                </div>
              </AlignLeftWrapper>
            </Grid>
            <Grid item xs={3} lg={2}>
              <AlignRightWrapper>
                <Tooltip title="Detail" placement="left">
                  <IconButton
                    color="accent"
                    onClick={() => {
                      history.push(`feeds/detail/${this.props.id}`);
                    }}
                  >
                    <i className="material-icons">visibility</i>
                  </IconButton>
                </Tooltip>
              </AlignRightWrapper>
            </Grid>
            <Grid item xs={true} sm={2} lg={2} />
            <Grid item xs={12} sm={10} lg={10}>
              <AlignLeftWrapper>
                <PrimaryColorWrapper>
                  <TextWrapper>{this.props.text}</TextWrapper>
                </PrimaryColorWrapper>
              </AlignLeftWrapper>
            </Grid>
          </Grid>
        </PaperStyled>
      </Grid>
    );
  }
}

export default Card;
