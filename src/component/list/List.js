import React, { Component } from "react";
import Card from "component/list/Card.js";
import Grid from "material-ui/Grid";
import styled from "styled-components";

const ContentWrapper = styled.div`
  padding: 15px;
`;

class List extends Component {
  render() {
    return (
      <ContentWrapper>
        <Grid container spacing={24}>
          {this.props.feeds.map((feed, index) => {
            return (
              <Card
                id={feed._id}
                key={feed._id}
                firstName={feed.person.firstName}
                lastName={feed.person.lastName}
                avatarSource={feed.person.avatar}
                text={feed.text}
                timestamp={feed.date}
              />
            );
          })}
        </Grid>
      </ContentWrapper>
    );
  }
}

export default List;
