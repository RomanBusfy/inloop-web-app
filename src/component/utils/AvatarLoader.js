import React, { Component } from "react";
import Avatar from "material-ui/Avatar";
import styled from "styled-components";
import ProgressiveImage from "react-progressive-image-loading";
import noImage from "img/avatar.gif";

const StyledAvatar = styled(Avatar)`
  && {
    width: 100%;
    height: auto;
    max-width: 150px;
  }
`;

const AvatarWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

class AvatarLoader extends Component {
  render() {
    return (
      <ProgressiveImage
        preview={noImage}
        src={this.props.src}
        transitionTime={500}
        transitionFunction="ease"
        render={src => (
          <AvatarWrapper>
            <StyledAvatar alt="" src={src} />
          </AvatarWrapper>
        )}
      />
    );
  }
}

export default AvatarLoader;
