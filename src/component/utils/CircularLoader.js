import React, { Component } from "react";
import { CircularProgress } from "material-ui/Progress";
import styled from "styled-components";

const CenteredDiv = styled.div`
  position: fixed;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%) }
`;

class Circular extends Component {
  render() {
    return (
      <CenteredDiv>
        {" "}
        <CircularProgress
          style={{ color: this.props.color }}
          size={this.props.size}
          thickness={this.props.thickness}
        />
      </CenteredDiv>
    );
  }
}

export default Circular;
