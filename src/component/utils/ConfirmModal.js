import React, { Component } from "react";
import Button from "material-ui/Button";
import Dialog, { DialogActions, DialogTitle } from "material-ui/Dialog";
import { CircularProgress } from "material-ui/Progress";

class ConfirmModal extends Component {
  constructor() {
    super();

    this.state = {
      isLoading: false
    };
  }

  onConfirm() {
    if (this.state.isLoading) {
      return;
    }

    this.setState({ isLoading: true }, () => {
      this.props.onConfirm(() => {
        this.setState({ isLoading: false });
      });
    });
  }

  render() {
    return (
      <div>
        <Dialog open={this.props.open}>
          <DialogTitle>{this.props.message}</DialogTitle>
          <DialogActions>
            <Button
              id="cy-close-button"
              onClick={() => {
                this.props.onClose();
              }}
              color="primary"
            >
              Disagree
            </Button>
            <Button
              id="cy-confirm-button"
              onClick={() => {
                this.onConfirm();
              }}
              color="accent"
              raised
            >
              Agree
              {this.state.isLoading ? (
                <span>
                  &nbsp;
                  <CircularProgress color="primary" size={20} />
                </span>
              ) : null}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default ConfirmModal;
