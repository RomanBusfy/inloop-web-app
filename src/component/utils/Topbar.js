import React, { Component } from "react";
import { history } from "module/History.js";
import AppBar from "material-ui/AppBar";
import Toolbar from "material-ui/Toolbar";
import Typography from "material-ui/Typography";
import IconButton from "material-ui/IconButton";

class Topbar extends Component {
  render() {
    return (
      <AppBar position="static" color="primary">
        <Toolbar>
          {this.props.button ? (
            <IconButton
              color="contrast"
              onClick={() => {
                history.push("/feeds");
              }}
            >
              <i className="material-icons">reply</i>
            </IconButton>
          ) : null}
          <Typography type="title" color="inherit">
            {this.props.title}
          </Typography>
        </Toolbar>
      </AppBar>
    );
  }
}
export default Topbar;
