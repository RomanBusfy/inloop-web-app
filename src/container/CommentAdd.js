import React, { Component } from "react";
import { addCommentToFeed } from "module/Api.js";
import { connect } from "react-redux";
import { notification as notificationActions } from "actions/index.js";
import CommentForm from "component/detail/CommentForm.js";

class CommentAdd extends Component {
  handleAddNewComment(firstName, lastName, comment) {
    return addCommentToFeed(this.props.id, comment, firstName, lastName)
      .then(() => {
        this.props.commentWasAdded();
        this.props.dispatch(
          notificationActions.add("Comment was successfly added!")
        );
      })
      .catch(error => {
        this.props.dispatch(notificationActions.add(error.message));
      });
  }

  render() {
    return (
      <CommentForm
        onSubmit={(firstName, lastName, comment) => {
          return this.handleAddNewComment(firstName, lastName, comment);
        }}
      />
    );
  }
}

CommentAdd = connect(state => {
  return {};
})(CommentAdd);

export default CommentAdd;
