import React, { Component } from "react";
import { deleteComment } from "module/Api.js";
import { connect } from "react-redux";
import { notification as notificationActions } from "actions/index.js";
import List from "component/detail/CommentsList.js";

class CommentsList extends Component {
  handleDeleteComment(feedId, commentId) {
    return deleteComment(feedId, commentId)
      .then(() => {
        this.props.commentWasDeleted();
        this.props.dispatch(
          notificationActions.add("Comment was successfully deleted!")
        );
      })
      .catch(error => {
        this.props.dispatch(notificationActions.add(error.message));
      });
  }

  render() {
    return (
      <List
        comments={this.props.comments}
        onItemDeleteRequest={commentId => {
          return this.handleDeleteComment(this.props.feedId, commentId);
        }}
      />
    );
  }
}

CommentsList = connect(state => {
  return {};
})(CommentsList);

export default CommentsList;
