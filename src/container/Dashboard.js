import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import Feeds from "container/Feeds.js";
import FeedDetail from "container/FeedDetail.js";

class Dashboard extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/feeds" component={Feeds} />
        <Route exact path="/feeds/detail/:id" component={FeedDetail} />
        <Route exact path="/*" component={Feeds} />
      </Switch>
    );
  }
}

export default Dashboard;
