import React, { Component } from "react";
import { getDetailOfFeed } from "module/Api.js";
import { connect } from "react-redux";
import { notification as notificationActions } from "actions/index.js";
import Topbar from "component/utils/Topbar.js";
import Detail from "component/detail/Detail.js";
import CircularLoader from "component/utils/CircularLoader.js";
import { primary as primaryColor } from "module/Colors.js";

class FeedDetail extends Component {
  constructor() {
    super();

    this.state = {
      feedDetail: null
    };
  }

  componentDidMount() {
    this.getDetailOfFeed();
  }

  getDetailOfFeed() {
    getDetailOfFeed(this.props.match.params.id)
      .then(feed => {
        this.setState({ feedDetail: feed });
      })
      .catch(error => {
        this.props.dispatch(notificationActions.add(error.message));
      });
  }
  render() {
    if (this.state.feedDetail === null) {
      return <CircularLoader color={primaryColor} size={100} thickness={3} />;
    }
    return (
      <div>
        <Topbar title="Detail" button={true} />
        <Detail
          feedDetail={this.state.feedDetail}
          commentWasAdded={() => {
            this.getDetailOfFeed();
          }}
          commentWasDeleted={() => {
            this.getDetailOfFeed();
          }}
        />
      </div>
    );
  }
}

FeedDetail = connect(state => {
  return {};
})(FeedDetail);

export default FeedDetail;
