import React, { Component } from "react";
import List from "component/list/List.js";
import { getAllFeeds } from "module/Api.js";
import { connect } from "react-redux";
import { notification as notificationActions } from "actions/index.js";
import Topbar from "component/utils/Topbar.js";
import CircularLoader from "component/utils/CircularLoader.js";
import { primary as primaryColor } from "module/Colors.js";

class Feeds extends Component {
  constructor() {
    super();

    this.state = {
      feeds: null
    };
  }

  componentDidMount() {
    this.getAllFeeds();
  }

  getAllFeeds() {
    getAllFeeds()
      .then(feeds => {
        this.setState({ feeds: feeds });
      })
      .catch(error => {
        this.props.dispatch(notificationActions.add(error.message));
      });
  }

  render() {
    if (!this.state.feeds) {
      return <CircularLoader color={primaryColor} size={100} thickness={3} />;
    }
    return (
      <div>
        <Topbar title="List" button={false} />
        <List feeds={this.state.feeds} />
      </div>
    );
  }
}

Feeds = connect(state => {
  return {};
})(Feeds);

export default Feeds;
