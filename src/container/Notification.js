import React, { Component } from "react";
import { connect } from "react-redux";
import SnackbarMaterial from "material-ui/Snackbar";
import { notification as notificationActions } from "actions/index.js";

class Notification extends Component {
  onClose() {
    this.props.dispatch(notificationActions.pop());
  }

  render() {
    if (this.props.notifications.length === 0) {
      return null;
    }
    return (
      <div>
        <SnackbarMaterial
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
          }}
          open={true}
          onClose={() => {
            this.onClose();
          }}
          autoHideDuration={3000}
          SnackbarContentProps={{
            "aria-describedby": "message-id"
          }}
          message={this.props.notifications[0]}
          action={
            <i
              className="material-icons"
              onClick={() => {
                this.onClose();
              }}
            >
              cancel
            </i>
          }
        />
      </div>
    );
  }
}

Notification = connect(state => {
  return {
    notifications: state.notification.notifications
  };
})(Notification);

export default Notification;
