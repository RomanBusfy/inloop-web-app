import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import reducer from "reducers/index.js";
import { createStore } from "redux";
import { Provider as ReduxProvider } from "react-redux";
import { withRouter, Router } from "react-router-dom";
import { history } from "module/History.js";
import { MuiThemeProvider } from "material-ui/styles";
import theme from "module/Theme.js";

const reduxStore = createStore(reducer);
const AppWithRouter = withRouter(App);

ReactDOM.render(
  <ReduxProvider store={reduxStore}>
    <MuiThemeProvider theme={theme}>
      <Router history={history}>
        <AppWithRouter />
      </Router>
    </MuiThemeProvider>
  </ReduxProvider>,
  document.getElementById("root")
);
registerServiceWorker();
