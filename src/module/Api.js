import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "https://inloop-webproject.herokuapp.com/api/",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  },
  validateStatus: status => status >= 200 && status < 500
});

export const getAllFeeds = () => {
  return axiosInstance
    .get("feeds")
    .then(response => {
      if (response.status === 200) {
        return response.data;
      }
      throw new Error(response.statusText);
    })
    .catch(error => {
      throw new Error(error.message);
    });
};

export const getDetailOfFeed = id => {
  return axiosInstance
    .get(`feeds/${id}`)
    .then(response => {
      if (response.status === 200) {
        return response.data;
      }
      throw new Error(response.statusText);
    })
    .catch(error => {
      throw new Error(error.message);
    });
};

export const addCommentToFeed = (id, comment, firstName, lastName) => {
  return axiosInstance
    .post(`feeds/${id}/comments`, {
      text: comment,
      person: {
        firstName: firstName,
        lastName: lastName
      }
    })
    .then(response => {
      if (response.status === 201) {
        return null;
      }
      throw new Error(response.statusText);
    })
    .catch(error => {
      throw new Error(error.message);
    });
};

export const deleteComment = (feedId, commentId) => {
  return axiosInstance
    .delete(`feeds/${feedId}/comments/${commentId}`)
    .then(response => {
      if (response.status === 204) {
        return;
      }
      throw new Error(response.statusText);
    })
    .catch(error => {
      throw new Error(error.message);
    });
};
