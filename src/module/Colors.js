export const primary = "#212738";
export const secondary = "#0ECFF0";
export const tertiary = "#798085";
export const additional = "#FB5658";
