import createHistory from "history/createBrowserHistory";

let userConfirmation = (message, callback) => {
  callback(true);
};

const history = createHistory({
  getUserConfirmation: (message, callback) => {
    userConfirmation(message, callback);
  }
});

history.block((location, action) => {
  return "Are you sure?";
});

const registerUserConfirmation = customConfirmation => {
  userConfirmation = customConfirmation;
};

export { history, registerUserConfirmation };
