import { createMuiTheme } from "material-ui/styles";
import {
  primary as primaryColor,
  additional as aditionalColor
} from "module/Colors.js";

export default createMuiTheme({
  direction: "ltr",
  palette: {
    type: "light",
    primary: {
      "50": primaryColor,
      "100": primaryColor,
      "200": primaryColor,
      "300": primaryColor,
      "400": primaryColor,
      "500": primaryColor,
      "600": primaryColor,
      "700": primaryColor,
      "800": primaryColor,
      "900": primaryColor,
      A100: primaryColor,
      A200: primaryColor,
      A400: primaryColor,
      A700: primaryColor,
      contrastDefaultColor: "light"
    },
    secondary: {
      "50": aditionalColor,
      "100": aditionalColor,
      "200": aditionalColor,
      "300": aditionalColor,
      "400": aditionalColor,
      "500": aditionalColor,
      "600": aditionalColor,
      "700": aditionalColor,
      "800": aditionalColor,
      "900": aditionalColor,
      A100: aditionalColor,
      A200: aditionalColor,
      A400: aditionalColor,
      A700: aditionalColor,
      contrastDefaultColor: "light"
    }
  }
});
