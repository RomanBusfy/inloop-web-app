export const isEmpty = value => {
  return /^\s*$/.test(value);
};
